<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Helper_Api extends Koin_Payment_Helper_Data
{
    const PAYMENT_TYPE = 21;
    const TELEPHONE_TYPE = 2;
    const MOBILE_TYPE = 4;
    const ADDRESS_TYPE = 1;
    const DELIVERY_DAYS = 15;
    const DEFAULT_CATEGORY = 'Produtos';

    const REFUND_TYPE_FULL = '1';
    const REFUND_TYPE_PARTIAL = '2';

    const CODE_APPROVED = 200;
    const CODE_PROCESS_ERROR_RESPONSE = 500;

    protected $_approvedStatues = array(200, 312, 314);
    protected $_errorStatuses = array(998);
    protected $_helper;

    private $_regions = array(
        'ACRE' => 'AC',
        'ALAGOAS' => 'AL',
        'AMAPÁ' => 'AP',
        'AMAZONAS' => 'AM',
        'BAHIA' => 'BA',
        'CEARÁ' => 'CE',
        'DISTRITO FEDERAL' => 'DF',
        'ESPÍRITO SANTO' => 'ES',
        'GOIÁS' => 'GO',
        'MARANHÃO' => 'MA',
        'MATO GROSSO' => 'MT',
        'MATO GROSSO DO SUL' => 'MS',
        'MINAS GERAIS' => 'MG',
        'PARÁ' => 'PA',
        'PARAÍBA' => 'PB',
        'PARANÁ' => 'PR',
        'PERNAMBUCO' => 'PE',
        'PIAUÍ' => 'PI',
        'RIO DE JANEIRO' => 'RJ',
        'RIO GRANDE DO NORTE' => 'RN',
        'RIO GRANDE DO SUL' => 'RS',
        'RONDÔNIA' => 'RO',
        'RORAIMA' => 'RR',
        'SANTA CATARINA' => 'SC',
        'SÃO PAULO' => 'SP',
        'SERGIPE' => 'SE',
        'TOCANTINS' => 'TO'
    );

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     *
     * @return array
     */
    public function getTransactionData($payment)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $payment->getOrder();
        $result['FraudId'] = $payment->getAdditionalInformation('koin_fraud_id');
        $result['Reference'] = $order->getIncrementId();
        $result['Currency'] = Mage::getStoreConfig('currency/options/base');
        $result['Price'] = $this->_formatPrice($order->getBaseGrandTotal());
        $result['PaymentType'] = $payment->getAdditionalInformation('koin_payment_type') ?: self::PAYMENT_TYPE;
        $result['DiscountPercent'] = 0;//$this->_formatPrice((abs($order->getBaseDiscountAmount()) / $order->getBaseGrandTotal()) * 100);
        $result['DiscountValue'] = $this->_formatPrice(abs($order->getBaseDiscountAmount()));
        $result['IncreasePercent'] = 0.00;
        $result['IncreaseValue'] = 0.00;
        $result['Buyer'] = $this->_getBuyerData($order);
        $result['Shipping'] = $this->_getShippingData($order);
        $result['Items'] = $this->_getItemsData($order);
        return $result;
    }

    private function _formatPrice($value)
    {
        return (float)number_format($value, 2, '.', '');
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return array
     */
    private function _getBuyerData($order)
    {
        $result = array();
        $address = $order->getBillingAddress();
        $result['Name'] = $address->getFirstname() . ' ' . $address->getLastname();
        $result['Email'] = $order->getCustomerEmail();
        $result['Documents'] = array(array(
            'Key' => 'CPF', 'Value' => $order->getData('customer_' . $this->_getHelper()
                    ->getConfig('customer_taxvat_attribute'))
        ));
        $result['AdditionalInfo'] = array(array(
            'Key' => 'BirthDay', 'Value' => $order->getCustomerDob() ? substr($order->getCustomerDob(), 0, 10) : null
        ));
        $result['Phones'] = $this->_getCustomerPhones($order);
        $result['Address'] = $this->_getCustomerAddress($order);

        $ip = '127.0.0.1';
        if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        $result['Ip'] = $ip;

        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return array
     */
    private function _getCustomerPhones($order)
    {
        $result = array();
        $telephone = $order->getBillingAddress()
            ->getData($this->_getHelper()->getConfig('telephone_number_attribute'));
        if ($telephone) {
            $telephone = preg_replace('/[^0-9]/', '', $telephone);
            $areaCode = substr($telephone, 0, 2);
            $number = substr($telephone, 2, strlen($telephone) - 1);
            $result[] = array(
                'AreaCode' => $areaCode,
                'Number' => $number,
                'PhoneType' => self::TELEPHONE_TYPE
            );
        }
        $mobile = $order->getBillingAddress()
            ->getData($this->_getHelper()->getConfig('mobile_number_attribute'));
        if ($mobile) {
            $mobile = preg_replace('/[^0-9]/', '', $mobile);
            $areaCode = substr($mobile, 0, 2);
            $number = substr($mobile, 2, strlen($mobile) - 1);
            $result[] = array(
                'AreaCode' => $areaCode,
                'Number' => $number,
                'PhoneType' => self::MOBILE_TYPE
            );
        }
        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return array
     */
    private function _getCustomerAddress($order)
    {
        $address = $order->getIsVirtual()
            ? $order->getBillingAddress()
            : $order->getShippingAddress();

        $street = $this->_getHelper()->getConfig('address_street_attribute');
        $number = $this->_getHelper()->getConfig('address_number_attribute');
        $complement = $this->_getHelper()->getConfig('address_complement_attribute');
        $district = $this->_getHelper()->getConfig('address_district_attribute');

        $result = array();
        $result['AdressType'] = self::ADDRESS_TYPE;
        $result['ZipCode'] = $address->getPostcode();

        $result['Street'] = (strpos($street, 'street_') !== false)
            ? $address->getStreet(str_replace('street_', '', $street))
            : $address->getData($street);

        $result['Number'] = (strpos($number, 'street_') !== false)
            ? $address->getStreet(str_replace('street_', '', $number))
            : $address->getData($number);

        $result['Complement'] = (strpos($complement, 'street_') !== false)
            ? $address->getStreet(str_replace('street_', '', $complement))
            : $address->getData($complement);

        $result['District'] = (strpos($district, 'street_') !== false)
            ? $address->getStreet(str_replace('street_', '', $district))
            : $address->getData($district);

        $result['City'] = $address->getCity();
        $result['State'] = $this->_getRegionCode($address);
        $result['Country'] = $order->getBillingAddress()->getCountryModel()->getName();

        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order_Address $address
     *
     * @return array
     */
    private function _getRegionCode($address)
    {
        $regionCode = $address->getRegionCode();
        if (!$regionCode || strlen($regionCode) != 2) {
            $regionName = trim($address->getRegion());
            $regionCode = $regionName;
            if (strlen($regionName) != 2) {
                $regionName = strtoupper($regionName);
                $regionCode = isset($this->_regions[$regionName])
                    ? $this->_regions[$regionName]
                    : $this->decipherRegion($regionName);
            }
        }

        return $regionCode;
    }

    /**
     * In Brazil there's a possibility to record states un string
     * This method try to find the correct state
     * @param $region
     * @return string
     */
    public function decipherRegion($region)
    {
        $code = '';
        $region = Mage::helper('core')->removeAccents($region);

        switch ($region) {
            case 'ACRE':
                $code = 'AC';
                break;
            case 'ALAGOAS':
                $code = 'AL';
                break;
            case 'AMAPA':
            case 'AMAP':
                $code = 'AP';
                break;
            case 'AMAZONAS':
            case 'AMAZONA':
                $code = 'AM';
                break;
            case 'BAHIA':
            case 'BAIA':
                $code = 'BA';
                break;
            case 'CEARA':
                $code = 'CE';
                break;
            case 'DISTRITO FEDERAL':
                $code = 'DF';
                break;
            case 'ESPIRITO SANTO':
                $code = 'ES';
                break;
            case 'GOIAS':
                $code = 'GO';
                break;
            case 'MARANHAO':
                $code = 'MA';
                break;
            case 'MATO GROSSO':
                $code = 'MT';
                break;
            case 'MATO GROSSO DO SUL':
                $code = 'MS';
                break;
            case 'MINAS GERAIS':
            case 'MINAS':
                $code = 'MG';
                break;
            case 'PARA':
                $code = 'PA';
                break;
            case 'PARAIBA':
                $code = 'PB';
                break;
            case 'PARANA':
                $code = 'PR';
                break;
            case 'PERNAMBUCO':
            case 'PERNANBUCO':
                $code = 'PE';
                break;
            case 'PIAUI':
                $code = 'PI';
                break;
            case 'RIO DE JANEIRO':
            case 'RIO JANEIRO':
            case 'RIO':
                $code = 'RJ';
                break;
            case 'RIO GRANDE DO NORTE':
                $code = 'RN';
                break;
            case 'RIO GRANDE DO SUL':
                $code = 'RS';
                break;
            case 'RONDONIA':
            case 'RONDONA':
                $code = 'RO';
                break;
            case 'RORAIMA':
                $code = 'RR';
                break;
            case 'SANTA CATARINA':
                $code = 'SC';
                break;
            case 'SAO PAULO':
                $code = 'SP';
                break;
            case 'SERGIPE':
                $code = 'SE';
                break;
            case 'TOCANTINS':
                $code = 'TO';
                break;
        }

        return $code;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return array
     */
    private function _getShippingData($order)
    {
        $result = array();
        $result['Address'] = $this->_getCustomerAddress($order);
        $result['DeliveryDate'] = $order->getCreatedAtStoreDate()
                ->addDay(self::DELIVERY_DAYS)->toString('y-MM-dd') . ' 00:00:00';
        $result['Price'] = $this->_formatPrice($order->getShippingAmount());
        return $result;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     *
     * @return array
     */
    private function _getItemsData($order)
    {
        $result = array();
        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($order->getAllVisibleItems() as $item) {
            $category = $item->getProduct()->getCategory()
                ? $item->getProduct()->getCategory()->getName()
                : self::DEFAULT_CATEGORY;
            $itemResult = array();
            $itemResult['Reference'] = $item->getSku();
            $itemResult['Description'] = $item->getName();
            $itemResult['Category'] = $category;
            $itemResult['Quantity'] = intval($item->getQtyOrdered());
            $itemResult['Price'] = $this->_formatPrice($item->getBasePrice());
            $result[] = $itemResult;
        }
        return $result;
    }

    /**
     * Verify if the status is different from pending, and then update the order
     *
     * @param $koinStatus
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param Mage_Sales_Model_Order $order
     *
     * @return boolean
     */
    public function setStatus($koinStatus, $payment, $order)
    {
        $originalStatus = $order->getStatus();
        $status = false;

        if ($this->isApproved($koinStatus)) {
            if ($koinStatus == self::CODE_APPROVED) {
                $status = $order->getConfig()->getStateDefaultStatus(Mage_Sales_Model_Order::STATE_PROCESSING);
                if ($this->getConfig('create_invoice')) {
                    $this->_createInvoice($order);
                } else {
                    $order->setState(Mage_Sales_Model_Order::STATE_PROCESSING);
                }
            } else {
                $status = $this->getConfig('in_analysis_status');
            }
        } elseif ($this->isDisapproved($koinStatus)) {
            $status = $this->getConfig('disapproved_status');
            $this->_cancelOrder($order);
        }

        if ($status && $originalStatus != $status) {
            $message = $payment->getAdditionalInformation('koin_message');
            $order->addStatusHistoryComment($message, $status)
                ->save();
        }
    }

    /**
     * Approved statues
     * @param $status
     * @return bool
     */
    public function isApproved($status)
    {
        return in_array($status, $this->_approvedStatues);
    }

    /**
     * Disaprovved Statuses
     * @param $status
     * @return bool
     */
    public function isDisapproved($status)
    {
        return !in_array($status, $this->_approvedStatues) && !in_array($status, $this->_errorStatuses);
    }

    /**
     * Verify if the order status is different from current status, if then update
     * @param $status
     * @param $orderKoinStatus
     * @return bool
     */
    public function shouldUpdate($status, $orderKoinStatus)
    {
        return ($status != $orderKoinStatus);
    }

    /**
     * Method that creates an invoice if doesn't exist
     * @param Mage_Sales_Model_Order $order
     */
    protected function _createInvoice(Mage_Sales_Model_Order &$order)
    {
        if (!$order->hasInvoices()) {
            $invoice = $order->prepareInvoice();
            $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_OFFLINE);
            $invoice->register()->pay();
            $msg = $this->_getHelper()->__('O pagamento foi aprovado pela KOIN Pós-Pago.');
            $invoice->addComment($msg);
            $invoice->sendEmail(true, $this->_getHelper()->__('O pagamento foi aprovado pela KOIN Pós-Pago.'));
            Mage::getModel('core/resource_transaction')
                ->addObject($invoice)
                ->addObject($invoice->getOrder())
                ->save();
        }
    }

    /**
     * Method that creates an invoice if doesn't exist
     * @param Mage_Sales_Model_Order $order
     */
    protected function _cancelOrder(Mage_Sales_Model_Order &$order)
    {
        if ($order->canCancel()) {
            $order->cancel();
            $order->save();
        }
    }

    /**
     * @return Koin_Payment_Helper_Data|Mage_Core_Helper_Abstract
     */
    private function _getHelper()
    {
        if (!$this->_helper) {
            /** @var Koin_Payment_Helper_Data _helper */
            $this->_helper = Mage::helper('koin');
        }
        return $this->_helper;
    }

}
<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_PAYMENT_CONSUMER_KEY = 'payment/koin/consumer_key';
    const XML_PATH_PAYMENT_SECRET_KEY = 'payment/koin/secret_key';
    const XML_PATH_PAYMENT_DEBUG = 'payment/koin/debug';
    const XML_PATH_PAYMENT_SANDBOX = 'payment/koin/sandbox';

    const XML_PATH_PAYMENT_URL_TRANSACTION = 'payment/koin/url_transaction';
    const XML_PATH_PAYMENT_SANDBOX_URL_TRANSACTION = 'payment/koin/sandbox_url_transaction';

    const XML_PATH_PAYMENT_URL_QUERY_INSTALLMENTS = 'payment/koin/url_query_installments';
    const XML_PATH_PAYMENT_SANDBOX_URL_QUERY_INSTALLMENTS = 'payment/koin/sandbox_url_query_installments';

    const XML_PATH_PAYMENT_URL_QUERY_STATUS = 'payment/koin/url_query_status';
    const XML_PATH_PAYMENT_SANDBOX_URL_QUERY_STATUS = 'payment/koin/sandbox_url_query_status';

    const XML_PATH_PAYMENT_URL_QUERY_LIMIT = 'payment/koin/url_query_limit';
    const XML_PATH_PAYMENT_SANDBOX_URL_QUERY_LIMIT = 'payment/koin/sandbox_url_query_limit';

    const XML_PATH_PAYMENT_URL_SEND_TRACKING = 'payment/koin/url_send_tracking';
    const XML_PATH_PAYMENT_SANDBOX_URL_SEND_TRACKING = 'payment/koin/sandbox_url_send_tracking';

    const XML_PATH_PAYMENT_URL_REVERSE_TRANSACTION = 'payment/koin/url_reverse_transaction';
    const XML_PATH_PAYMENT_SANDBOX_URL_REVERSE_TRANSACTION = 'payment/koin/sandbox_url_reverse_transaction';

    const XML_PATH_PAYMENT_URL_CREATE_TICKET = 'payment/koin/url_create_ticket';
    const XML_PATH_PAYMENT_SANDBOX_URL_CREATE_TICKET = 'payment/koin/sandbox_url_create_ticket';

    public function getUrl($name)
    {
        $configPath = constant('self::XML_PATH_PAYMENT' .
            ($this->isSandbox() ? '_SANDBOX' : '')
            . '_URL_' . strtoupper($name));
        return Mage::getStoreConfig($configPath);
    }

    public function isSandbox()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PAYMENT_SANDBOX);
    }

    public function getConsumerKey()
    {
        return Mage::getStoreConfig(self::XML_PATH_PAYMENT_CONSUMER_KEY);
    }

    public function getSecretKey()
    {
        return Mage::getStoreConfig(self::XML_PATH_PAYMENT_SECRET_KEY);
    }

    public function log($message)
    {
        if ($this->isDebugActive()) {
            Mage::log($message, Zend_Log::DEBUG, 'koin.log', true);
        }
    }

    public function isDebugActive()
    {
        return Mage::getStoreConfigFlag(self::XML_PATH_PAYMENT_DEBUG);
    }

    /**
     * @return Mage_Adminhtml_Model_Session_Quote | Mage_Checkout_Model_Session
     */
    public function getSession()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return Mage::getSingleton('adminhtml/session_quote');
        } else {
            return Mage::getSingleton('checkout/session');
        }
    }

    public function getConfig($config, $paymentMethod = 'koin')
    {
        return Mage::getStoreConfig('payment/' . $paymentMethod . '/' . $config);
    }


}
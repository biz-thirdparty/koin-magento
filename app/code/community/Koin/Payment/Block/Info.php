<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Block_Info extends Mage_Payment_Block_Info
{
    public function getTicketLink($orderId)
    {
        return sprintf(Mage::helper('koin')->getUrl('create_ticket'), Mage::helper('koin')->getConsumerKey(), $orderId);
    }

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('koin/info.phtml');
    }

    /**
     * Render as PDF
     * @return string
     */
    public function toPdf()
    {
        $this->setTemplate('koin/pdf/info.phtml');
        return $this->toHtml();
    }
}
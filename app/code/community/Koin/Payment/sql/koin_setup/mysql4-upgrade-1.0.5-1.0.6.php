<?php
$installer = $this;

$installer->startSetup();

//Update the name of "Saffira_Koin" in the payment_method tables to safely disable the former official KOIN Module
$table = $this->getTable('sales_flat_order');
if ($installer->getConnection()->tableColumnExists($table, 'payment_method')) {
    $installer->run("UPDATE `{$table}` SET `payment_method` = 'koin' WHERE `payment_method` = 'Saffira_Koin_Standard'");
}

$table = $this->getTable('sales_flat_order_payment');
if ($installer->getConnection()->tableColumnExists($table, 'method')) {
    $installer->run("UPDATE `{$table}` SET `method` = 'koin' WHERE `method` = 'Saffira_Koin_Standard';");
}

$installer->endSetup();
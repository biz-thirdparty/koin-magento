<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Adminhtml_KoinController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/sales');
    }

    public function statusAction()
    {
        /** @var $helper Koin_Payment_Helper_Data */
        $helper = Mage::helper('koin');
        $reloadAction = null;

        $body = $helper->__('Não foi possível consultar a Transa&ccedil;&atilde;o');

        try {
            $orderId = $this->getRequest()->getParam('order_id');
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);

            if ($order && $order->getId()) {

                /** @var Koin_Payment_Helper_Api $helperApi */
                $helperApi = Mage::helper('koin/api');

                /** @var Koin_Payment_Model_Service_Api $api */
                $api = Mage::getModel('koin/service_api');

                $response = $api->queryStatus(array('Reference' => $order->getRealOrderId()));
                if ($response && $response->getCode()) {

                    if (property_exists($response->getBody(), 'Status')) {
                        $orderKoinStatus = $order->getPayment()->getAdditionalInformation('koin_code');
                        if ($helperApi->shouldUpdate($response->getCode(), $orderKoinStatus)) {
                            $reloadAction = 'window.opener.location.reload();';
                            $order->getPayment()->setAdditionalInformation('koin_code', $response->getCode());
                            $order->getPayment()->setAdditionalInformation('koin_message', $response->getBody()->Status);
                            $helperApi->setStatus($response->getCode(), $order->getPayment(), $order);
                        }
                    }

                    $body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"><html xmlns="http://www.w3.org/1999/xhtml" xml:lang="pt" lang="pt"><head><title>Consulta Transação</title><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/></head>' .
                        '<body onunload="' . $reloadAction . '">' .
                        '<h1>' . $helper->__('Consulta Transa&ccedil;&atilde;o') . '</h1>' .
                        '<textarea cols="40" rows="10" readonly="readonly"> ' . json_encode($response->getBody()) . '</textarea><br />' .
                        '<br />' .
                        '<button onclick="' . $reloadAction . 'window.close();">Fechar Janela</button>';
                }


            }
        } catch (Exception $e) {
            $helper->log('Error: ' . $e->getMessage());
            $helper->log('Trace: ' . $e->getTraceAsString());
        }
        $this->getResponse()->setBody($body);
    }

}
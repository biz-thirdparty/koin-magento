<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Source_Customer_Attributes
{
    protected $_fontendTypes = array('text', 'textarea', 'select');

    /**
     * Retrieves attributes
     *
     * @return array
     */
    public function toOptionArray()
    {
        $attributes = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setEntityTypeFilter(Mage::getModel('customer/customer')->getEntityTypeId());

        $result = array();
        $result[] = array(
            'value' => '',
            'label' => Mage::helper('adminhtml')->__('-- Please Select --')
        );
        foreach ($attributes as $attribute) {
            /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            if ($attribute->getId() && in_array($attribute->getFrontendInput(), $this->_fontendTypes)) {
                $result[] = array(
                    'value' => $attribute->getAttributeCode(),
                    'label' => Mage::helper('adminhtml')->__($attribute->getFrontend()->getLabel()) . ' (' . $attribute->getAttributeCode() . ')',
                );
            }
        }
        return $result;
    }

}
<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Source_Environment
{
    /**
     * Retrieves attributes
     *
     * @return array
     */
    public function toOptionArray()
    {


        $result = array(
            array(
                'value' => '1',
                'label' => Mage::helper('adminhtml')->__('Teste')
            ),
            array(
                'value' => '0',
                'label' => Mage::helper('adminhtml')->__('Produção')
            ),
        );
        return $result;
    }
}

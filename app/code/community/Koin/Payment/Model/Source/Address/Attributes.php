<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Source_Address_Attributes
{
    /**
     * @return array
     */
    public function toArray()
    {
        $ret = array();
        foreach ($this->toOptionArray() as $arr) {
            $ret[$arr['value']] = $arr['label'];
        }
        return $ret;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $entityType = Mage::getModel('eav/config')->getEntityType('customer_address');
        $entityTypeId = $entityType->getEntityTypeId();
        $fields = Mage::getResourceModel('eav/entity_attribute_collection')->setEntityTypeFilter($entityTypeId);
        $options = array();
        foreach ($fields as $key => $value) {
            if (!is_null($value['frontend_label'])) {
                if ($value['attribute_code'] == 'street') {
                    $streetLines = Mage::getStoreConfig('customer/address/street_lines');
                    for ($i = 1; $i <= $streetLines; $i++) {
                        $options[] = array(
                            'value' => 'street_' . $i,
                            'label' => Mage::helper('koin')->__('Street Line %d', $i));
                    }
                } else {
                    $options[] = array(
                        'value' => $value['attribute_code'],
                        'label' => Mage::helper('adminhtml')->__($value['frontend_label']) . ' (' . $value['attribute_code'] . ')'
                    );
                }
            }
        }
        return $options;
    }

}
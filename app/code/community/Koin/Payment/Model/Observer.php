<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Observer
{

    /**
     * Method that create status when code is approved
     * @param Varien_Event_Observer $event
     * @return $this
     */
    public function processPayment(Varien_Event_Observer $event)
    {
        try {
            /** @var Mage_Sales_Model_Order_Payment $payment */
            $payment = $event->getPayment();
            /** @var Mage_Sales_Model_Order $order */
            $order = $payment->getOrder();

            /** @var $helperApi Koin_Payment_Helper_Api */
            $helperApi = Mage::helper('koin/api');

            $paymentMethod = $payment->getMethod();
            if ($paymentMethod == Koin_Payment_Model_Method::METHOD) {
                $status = $payment->getAdditionalInformation('koin_code');
                $helperApi->setStatus($status, $payment, $order);
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $this;
    }

    /**
     * Charge back when an order is cancelled
     * @param Varien_Event_Observer $event
     * @return $this
     */
    public function refundPayment(Varien_Event_Observer $event)
    {
        try {
            if (!Mage::registry('koin_refund_payment')) {

                //Create a register to not send twice the same registry
                Mage::register('koin_refund_payment', true);

                /** @var $helper Koin_Payment_Helper_Data */
                $helper = Mage::helper('koin');

                if (!$helper->getConfig('active') || !$helper->getConfig('refund_payment'))
                    return $this;

                /** @var Mage_Sales_Model_Order_Payment $payment */
                $payment = $event->getPayment();

                /** @var Mage_Sales_Model_Order $order */
                $_order = $payment->getOrder();

                if ($payment->getMethod() == Koin_Payment_Model_Method::METHOD) {
                    $this->_refund($_order);
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }

        return $this;
    }

    /**
     * Refund a order when analy
     * @param Varien_Event_Observer $event
     * @return $this
     */
    public function cancelPayment(Varien_Event_Observer $event)
    {
        try {
            if (!Mage::registry('koin_refund_payment')) {

                //Create a register to not send twice the same registry
                Mage::register('koin_refund_payment', true);

                /** @var $helper Koin_Payment_Helper_Data */
                $helper = Mage::helper('koin');

                if (!$helper->getConfig('active') || !$helper->getConfig('refund_payment'))
                    return $this;

                /** @var Mage_Sales_Model_Order $_order */
                $_order = $event->getOrder();

                /** @var Mage_Sales_Model_Order_Payment $payment */
                $_payment = $_order->getPayment();

                if ($_payment->getMethod() == Koin_Payment_Model_Method::METHOD) {
                    $this->_refund($_order, true);
                }
            }
        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $this;
    }

    /**
     * Reverse transaction at KOIN
     * @param Mage_Sales_Model_Order $_order
     * @param bool $cancel
     * @return $this
     */
    protected function _refund(Mage_Sales_Model_Order $_order, $cancel = false)
    {
        try {
            $type = ($_order->getBaseGrandTotal() == $_order->getBaseTotalRefunded()) ? Koin_Payment_Helper_Api::REFUND_TYPE_FULL : Koin_Payment_Helper_Api::REFUND_TYPE_PARTIAL;
            $value = ($_order->getBaseGrandTotal() == $_order->getBaseTotalRefunded()) ? null : number_format($_order->getBaseTotalRefunded(), 2, '.', '');

            //If the action is canceling a payment
            if ($cancel) {
                $type = Koin_Payment_Helper_Api::REFUND_TYPE_FULL;
                $value = 0;
            }

            /** @var Koin_Payment_Model_Service_Api $api */
            $api = Mage::getModel('koin/service_api');
            $api->reverseTransaction(array(
                'Reference' => $_order->getRealOrderId(),
                'Type' => $type,
                'Value' => $value,
                'AdditionalInfo' => Mage::helper('koin/api')->__('Pedido cancelado na Loja Virtual')
            ));
        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $this;
    }

    /**
     * Send tracking code to KOIN
     * @param $event
     * @return $this
     */
    public function sendTracking($event)
    {
        try {
            /** @var $helper Koin_Payment_Helper_Data */
            $helper = Mage::helper('koin');
            if (!$helper->getConfig('active') || !$helper->getConfig('send_tracking'))
                return $this;

            /** @var Mage_Sales_Model_Order_Shipment_Track $track */
            $track = $event->getTrack();

            /** @var Mage_Sales_Model_Order_Payment $payment */
            $payment = $track->getShipment()->getOrder()->getPayment();

            /** @var Mage_Sales_Model_Order $order */
            $order = $track->getShipment()->getOrder();

            if ($payment->getMethod() == Koin_Payment_Model_Method::METHOD) {
                /** @var Koin_Payment_Model_Service_Api $api */
                $api = Mage::getModel('koin/service_api');
                $api->sendTracking(array(
                    'Reference' => $order->getRealOrderId(),
                    'TrackingCode' => $track->getNumber(),
                    'CarrierCompany' => $track->getTitle(),
                ));
            }

        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $this;
    }

}
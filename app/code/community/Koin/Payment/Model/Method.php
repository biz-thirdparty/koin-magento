<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Method extends Mage_Payment_Model_Method_Abstract
{
    const METHOD = 'koin';

    protected $_code = self::METHOD;
    protected $_formBlockType = 'koin/form';
    protected $_infoBlockType = 'koin/info';
    protected $_isGateway = true;
    protected $_canUseInternal = true;
    protected $_canUseCheckout = true;
    protected $_canUseForMultishipping = true;
    protected $_canRefund = true;
    protected $_canRefundInvoicePartial = true;
    protected $_canVoid = false;
    protected $_canCapture = true;
    protected $_canCapturePartial = true;
    protected $_canCaptureOnce = true;

    protected $_helper;


    public function assignData($data)
    {
        if (!($data instanceof Varien_Object)) {
            $data = new Varien_Object($data);
        }
        $info = $this->getInfoInstance();
        $info->setAdditionalInformation('koin_fraud_id', $data->getKoinFraudId());
        $info->setAdditionalInformation('koin_payment_type', $data->getKoinPaymentType());

        //Installment values
        $info->setAdditionalInformation('koin_original_value', $data->getData('koin_original_value'));
        $info->setAdditionalInformation('koin_installments', $data->getData('koin_installments'));
        $info->setAdditionalInformation('koin_installment_value', $data->getData('koin_installment_value'));
        $info->setAdditionalInformation('koin_order_value', $data->getData('koin_order_value'));
        $info->setAdditionalInformation('koin_first_due_date', $data->getData('koin_first_due_date'));
        $info->setAdditionalInformation('koin_rates', $data->getData('koin_rates'));
        $info->setAdditionalInformation('koin_iof', $data->getData('koin_iof'));
        $info->setAdditionalInformation('koin_cet', $data->getData('koin_cet'));

        return $this;
    }

    /**
     * Order payment
     *
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param float $amount
     *
     * @return $this
     */
    public function order(Varien_Object $payment, $amount)
    {
        /** @var Koin_Payment_Helper_Api $helper */
        $helper = Mage::helper('koin/api');
        try {
            /** @var Koin_Payment_Model_Service_Api $api */
            $api = Mage::getModel('koin/service_api');
            $params = $helper->getTransactionData($payment);
            $response = $api->transaction($params);
            if (!$response) {
                Mage::throwException($helper->__('There was a communication problem with the KOIN.'));
            }
            if (!$helper->isApproved($response->getCode())) {
                Mage::getSingleton('checkout/session')->getQuote()->setReservedOrderId(null);
                if ($response->getCode() != Koin_Payment_Helper_Api::CODE_PROCESS_ERROR_RESPONSE) {
                    throw new Koin_Payment_Model_Service_Exception($response->getMessage(), $response->getCode());
                }
                Mage::throwException($response->getMesssage());
            }

            $additionalInfo = property_exists($response->getBody(), 'AdditionalInfo') ? $response->getBody()->AdditionalInfo : '';

            $payment->setAdditionalInformation('koin_status', $response->getStatus());
            $payment->setAdditionalInformation('koin_code', $response->getCode());
            $payment->setAdditionalInformation('koin_message', $response->getMessage());
            $payment->setAdditionalInformation('koin_additional_info', $additionalInfo);
            $payment->setSkipOrderProcessing(true);
        } catch (Koin_Payment_Model_Service_Exception $e) {
            $helper->log($helper->__('Not Approved.'));
            $helper->log($e->getMessage());
            Mage::throwException($e->getMessage());
        } catch (Exception $e) {
            $helper->log($helper->__('An error has occurred.'));
            $helper->log($e->getMessage());
            $helper->log($e->getTraceAsString());
            Mage::throwException($helper->__('There was an error processing your request. Please contact us or try again later.'));
        }
        return $this;

    }

    /**
     * @param Mage_Sales_Model_Quote $quote
     *
     * @return bool
     */
    public function isAvailable($quote = null)
    {
        if (parent::isAvailable($quote)) {

            $available = true;

            $lowerThan = (float)$this->getConfigData('show_only_if_total_lower_than');
            if ($available && $lowerThan > 0) {
                $available = (float)$quote->getBaseGrandTotal() <= $lowerThan;
            }

            $greaterThan = (float)$this->getConfigData('show_only_if_total_greater_than');
            if ($available && $greaterThan > 0) {
                $available = (float)$quote->getBaseGrandTotal() >= $greaterThan;
            }

            //Verify if the type of buyer attribute is set and if it's possible to show
            if (
                $available
                && $this->getConfigData('hide_if_corporate')
                && $this->getConfigData('customer_attribute_type')
                && $this->getConfigData('customer_attribute_type_value_corporate')
            ) {
                $_attributeCode = $this->getConfigData('customer_attribute_type');
                if ($quote->getCustomer()->getId()) {
                    /** @var Mage_Customer_Model_Customer $_customer */
                    $_customer = Mage::getModel('customer/customer')->load($quote->getCustomer()->getId());
                    $customerType = $_customer->getData($this->getConfigData('customer_attribute_type'));
                    $available = ($customerType != $this->getConfigData('customer_attribute_type_value_corporate'));

                    //If the value is numeric, verify the text of attribute and compare
                    if (is_numeric($customerType) && $available) {
                        $_customerTypeValue = $_customer->getResource()->getAttribute($_attributeCode)->getFrontend()->getValue($_customer);
                        $available = ($customerType != $_customerTypeValue);
                    }
                } else {
                    $customerType = $quote->getData('customer_' . $_attributeCode);
                    $available = ($customerType != $this->getConfigData('customer_attribute_type_value_corporate'));
                }
            }

            if ($available && $this->getConfigData('hide_if_virtual')) {
                $cartItems = $quote->getAllVisibleItems();
                foreach ($cartItems as $item) {
                    $_productId = $item->getProductId();
                    /** @var Mage_Catalog_Model_Product $_product */
                    $_product = Mage::getModel('catalog/product')->load($_productId);
                    if ($_product->getTypeId() == 'virtual' || $_product->getTypeId() == 'downloadable') {
                        $available = false;
                    }
                }
            }

            if ($available) {

                $total = number_format($quote->getGrandTotal(), 2, '.', '');

                //Only with balance
                if ($this->getConfigData('show_only_if_balance')) {
                    $taxvatAttribute = $this->getConfigData('customer_taxvat_attribute');
                    $taxvat = $quote->getData('customer_' . $taxvatAttribute);
                    $customerEmail = $quote->getCustomerEmail();
                    if (!$taxvat && !$customerEmail) {
                        $billingData = Mage::app()->getRequest()->getParam('billing');
                        if ($billingData) {
                            $customerEmail = isset($billingData['email']) ? $billingData['email'] : null;
                            $taxvat = isset($billingData[$taxvatAttribute]) ? $billingData[$taxvatAttribute] : null;
                        } else {
                            $osc = Mage::getSingleton('checkout/session')->getData('onestepcheckout_form_values');
                            if ($osc && isset($osc['billing'])) {
                                $customerEmail = isset($osc['billing']['email']) ? $osc['billing']['email'] : null;
                                $taxvat = isset($osc['billing'][$taxvatAttribute]) ? $osc['billing'][$taxvatAttribute] : null;
                            }
                        }
                    }

                    /** @var Koin_Payment_Model_Service_Api $service */
                    $service = Mage::getModel('koin/service_api');
                    $response = $service->queryLimit(array(
                        'Cpf' => $taxvat,
                        'Email' => $customerEmail,
                        'Price' => $total
                    ));
                    $balance = 0;
                    if ($response->getCode() == Koin_Payment_Model_Service_Api::RETURN_STATUS_QUERY_LIMIT_OK) {
                        $body = $response->getBody();
                        $balance = (float)$body->CreditLimitAvailable;
                        if ($balance && property_exists($body, 'installmentOptions')) {
                            $installments = $body->installmentOptions;
                            $this->_getHelper()->getSession()->setKoinInstallments($installments);
                        }
                    }
                    $available = $balance >= (float)$quote->getBaseGrandTotal();

                } else { //All Customers

                    /** @var Koin_Payment_Model_Service_Api $service */
                    $service = Mage::getModel('koin/service_api');
                    $response = $service->queryInstallments(array(
                        'Price' => $total
                    ));

                    if ($response->getCode() == Koin_Payment_Model_Service_Api::RETURN_STATUS_QUERY_LIMIT_OK) {
                        $body = $response->getBody();
                        if (property_exists($body, 'installmentOptions')) {
                            $installments = $body->installmentOptions;
                            $this->_getHelper()->getSession()->setKoinInstallments($installments);
                        }
                    }

                }
            }

            return $available;
        }

        return false;
    }

    public function validate()
    {
        /** @var Koin_Payment_Helper_Api $helper */
        $helper = Mage::helper('koin/api');
        $taxvatAttribute = 'customer_' . $this->_getHelper()->getConfig('customer_taxvat_attribute');
        $quote = $helper->getSession()->getQuote();

        if (!$quote->getData($taxvatAttribute)) {

            Mage::throwException($helper->__('Please fill out the document number on your registration.'));

        } elseif (!$quote->getPayment()->getAdditionalInformation('koin_fraud_id')) {

            $action = Mage::app()->getRequest()->getActionName();
            if ($action == 'savePayment') {
                Mage::throwException($helper->__('There was an error processing your request. Please contact us or try again later.'));
            }

        }

        return parent::validate();
    }

    /**
     * @return Koin_Payment_Helper_Data|Mage_Core_Helper_Abstract
     */
    protected function _getHelper()
    {
        if (!$this->_helper) {
            $this->_helper = Mage::helper('koin');
        }

        return $this->_helper;
    }

}
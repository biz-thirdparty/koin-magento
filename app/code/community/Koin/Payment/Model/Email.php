<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Email
{

    const CONFIG_EMAIL_IDENTITY = 'trans_email/ident_sales';
    const EMAIL_TEMPLATE_SUGGEST_NEW_ORDER = 'suggest_new_order_email_template';
    const CONFIG_EMAIL_SUGGEST_NEW_ORDER_TEMPLATE = 'payment/koin/suggest_new_order_email_template';

    /**
     * Send Order Pending Payment Email
     *
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    public function sendEmailSuggestNewOrder(Mage_Sales_Model_Order $order)
    {

        $storeId = $order->getStoreId();
        $templateId = $this->_getConfigPendingPaymentTemplate($storeId);
        $customerName = $order->getCustomerName();
        $customerEmail = $order->getCustomerEmail();

        /* @var $translate Mage_Core_Model_Translate */
        $translate = Mage::getSingleton('core/translate');
        $translate->setTranslateInline(false);

        /* @var $mailTemplate Mage_Core_Model_Email_Template */
        $mailTemplate = Mage::getModel('core/email_template');

        $sender = Mage::getStoreConfig(self::CONFIG_EMAIL_IDENTITY, $storeId);

        $mailTemplate->setDesignConfig(array(
            'area' => 'frontend',
            'store' => $storeId,
        ));

        // Start store emulation process
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
        try {
            // Retrieve specified view block from appropriate design package (depends on emulated store)
            $paymentBlock = Mage::helper('payment')->getInfoBlock($order->getPayment())
                ->setIsSecureMode(true);
            $paymentBlock->getMethod()->setStore($storeId);
            $paymentBlockHtml = $paymentBlock->toHtml();
        } catch (Exception $exception) {
            // Stop store emulation process
            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $exception;
        }

        // Stop store emulation process
        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);

        $vars = array(
            'order' => $order,
            'payment_html' => $paymentBlockHtml,
        );

        return $mailTemplate->sendTransactional(
            $templateId,
            $sender,
            $customerEmail,
            $customerName,
            $vars,
            $storeId
        );
    }

    /**
     * Retrieves Config Template
     *
     * @param $storeId
     * @return mixed|string
     */
    protected function _getConfigPendingPaymentTemplate($storeId)
    {
        $config = Mage::getStoreConfig(self::CONFIG_EMAIL_SUGGEST_NEW_ORDER_TEMPLATE, $storeId);
        if (!$config || $config == 'payment_koin_suggest_new_order_email_template')
            $config = self::EMAIL_TEMPLATE_SUGGEST_NEW_ORDER;
        return $config;
    }


}
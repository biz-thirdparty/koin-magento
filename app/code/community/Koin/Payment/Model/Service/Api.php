<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Service_Api extends Koin_Payment_Model_Service_Abstract
{

    const RETURN_STATUS_QUERY_LIMIT_OK = '10100';

    public function transaction($params)
    {
        $requestParams['params'] = $params;
        $requestParams['url'] = $this->_getHelper()->getUrl('transaction');
        return $this->_doPostRequest($requestParams);
    }

    /**
     * @return Koin_Payment_Helper_Data
     */
    private function _getHelper()
    {
        return Mage::helper('koin');
    }

    public function queryStatus($params)
    {
        $requestParams['params'] = $params;
        $requestParams['url'] = $this->_getHelper()->getUrl('query_status');
        return $this->_doPostRequest($requestParams);
    }

    public function queryLimit($params)
    {
        $requestParams['params'] = $params;
        $requestParams['url'] = $this->_getHelper()->getUrl('query_limit');
        return $this->_doPostRequest($requestParams);
    }

    public function queryInstallments($params)
    {
        $requestParams['params'] = $params;
        $requestParams['url'] = $this->_getHelper()->getUrl('query_installments');
        return $this->_doPostRequest($requestParams);
    }

    public function sendTracking($params)
    {
        $requestParams['params'] = $params;
        $requestParams['url'] = $this->_getHelper()->getUrl('send_tracking');
        return $this->_doPostRequest($requestParams);
    }

    public function reverseTransaction($params)
    {
        $requestParams['params'] = $params;
        $requestParams['url'] = $this->_getHelper()->getUrl('reverse_transaction');
        return $this->_doPostRequest($requestParams);
    }

}
<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Service_Response extends Varien_Object
{

    public function __construct($httpCode, $body, $status, Zend_Http_Response $response = null)
    {
        $code = '';
        $message = '';
        if (is_object($body) && property_exists($body, 'Code')) {
            $code = $body->Code;
            $message = $body->Message;
        } elseif ($response && $response->getMessage()) {
            $code = $response->getStatus();
            $message = $response->getMessage();
        }
        parent::__construct(
            array(
                'http_code' => $httpCode,
                'body' => $body,
                'status' => $status,
                'code' => $code,
                'message' => '(' . $code . ') ' . $message,
            )
        );
    }

}
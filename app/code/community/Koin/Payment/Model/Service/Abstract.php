<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Service_Abstract extends Zend_Service_Abstract
{

    const METHOD_GET = Zend_Http_Client::GET;
    const METHOD_POST = Zend_Http_Client::POST;
    const METHOD_PUT = Zend_Http_Client::PUT;
    const RESPONSE_CODE_ERROR = '999';

    /**
     * @var string Last Error
     */
    private $_lastError;

    protected function _doPostRequest($params)
    {
        return $this->_doRequest($params, self::METHOD_POST);
    }

    private function _doRequest($params, $method)
    {
        $configs = array('timeout' => 30);
        try {
            $this->getHttpClient()->resetParameters(true);
            $this->_setAuthenticate($params['url']);
            $this->getHttpClient()->setRawData(json_encode($params['params']));
            $this->getHttpClient()->setHeaders('Content-Type', 'application/json; charset=utf-8');
            $this->getHttpClient()->setHeaders('Content-Length', strlen(json_encode($params['params'])));
            $this->getHttpClient()->setMethod($method);
            $this->getHttpClient()->setConfig($configs);
            $this->getHttpClient()->setUri($params['url']);
            $response = $this->getHttpClient()->request();
            $this->_saveRequestLog();
            return $this->_getResponse($response);
        } catch (Exception $e) {
            $this->_lastError = $e->getMessage();
            Mage::logException($e);
        }
        return $this->_getResponse();
    }

    private function _setAuthenticate($url)
    {
        $consumerKey = Mage::helper('koin')->getConsumerKey();
        $secretKey = Mage::helper('koin')->getSecretKey();
        $oldTimeZone = date_default_timezone_get();
        @date_default_timezone_set("UTC");
        $time = time();
        @date_default_timezone_set($oldTimeZone);
        $binaryHash = hash_hmac('sha512', $url . $time, $secretKey, true);
        $hash = base64_encode($binaryHash);
        $this->getHttpClient()->setHeaders('Authorization', "{$consumerKey},{$hash},{$time}");
    }

    private function _saveRequestLog()
    {
        Mage::helper('koin')->log('REQUEST');
        Mage::helper('koin')->log($this->getHttpClient()->getLastRequest());
        Mage::helper('koin')->log('RESPONSE');
        Mage::helper('koin')->log($this->getHttpClient()->getLastResponse());
    }

    private function _getResponse(Zend_Http_Response $response = null)
    {
        if (!$response) {
            return new Koin_Payment_Model_Service_Response(
                self::RESPONSE_CODE_ERROR,
                array('message' => $this->getLastError()),
                false,
                null
            );
        }
        $httpCode = $response->getStatus();
        $body = json_decode($response->getBody());
        if (json_last_error() === JSON_ERROR_NONE && !is_object($body)) {
            $body = json_decode($body);
        }
        $status = $response->getStatus() == 200;
        return new Koin_Payment_Model_Service_Response($httpCode, $body, $status, $response);
    }

    public function getLastError()
    {
        return $this->_lastError;
    }

}
<?php

/**
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL).
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category    Koin
 * @package     Koin_Payment
 * @copyright   Copyright (c) 2016 KOIN [www.koin.com.br]
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Koin_Payment_Model_Cron
{

    /**
     * Job lock properties
     */
    protected $_isLocked = array();
    protected $_lockFile = array();

    /**
     * Query status
     *
     * @return $this
     */
    public function queryStatus()
    {
        /** @var $helper Koin_Payment_Helper_Data */
        $helper = Mage::helper('koin');
        /** @var Koin_Payment_Helper_Api $helperApi */
        $helperApi = Mage::helper('koin/api');

        if (!$helper->getConfig('active'))
            return $this;

        $lockId = 'query_status';
        if ($this->isLocked($lockId)) {
            return $this;
        }
        $this->lock($lockId);

        $date = new DateTime('-15 DAYS'); // first argument uses strtotime parsing
        $fromDate = $date->format('Y-m-d');

        $states = array(
            Mage_Sales_Model_Order::STATE_CANCELED,
            Mage_Sales_Model_Order::STATE_COMPLETE,
            $helperApi->getConfig('disapproved_status')
        );

        /** @var Mage_Sales_Model_Resource_Order_Collection $orders */
        $orders = Mage::getModel('sales/order')->getCollection();
        $orders->join(
            array('payment' => 'sales/order_payment'),
            'main_table.entity_id=payment.parent_id',
            array('payment_method' => 'payment.method')
        );
        $orders->addFieldToFilter('payment.method', Koin_Payment_Model_Method::METHOD);

        if ($helper->getConfig('update_specific_statuses')) {
            $states = explode(',', $helper->getConfig('cron_statuses'));
            $orders->addFieldToFilter('state', array('in' => $states));
        } else {
            $orders->addFieldToFilter('state', array('nin' => $states));
        }


        $orders->addFieldToFilter('created_at', array('gt' => $fromDate));

        if (!$orders->count())
            return $this;

        /** @var Koin_Payment_Model_Service_Api $api */
        $api = Mage::getModel('koin/service_api');

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orders as $order) {
            try {
                if ($order && $order->getId()) {
                    $response = $api->queryStatus(array('Reference' => $order->getRealOrderId()));
                    if ($response && $response->getCode() && property_exists($response->getBody(), 'Status')) {
                        $order->getPayment()->setAdditionalInformation('koin_code', $response->getCode());
                        $order->getPayment()->setAdditionalInformation('koin_message', $response->getBody()->Status);
                        $helperApi->setStatus($response->getCode(), $order->getPayment(), $order);
                        $order->save();
                    }
                }
            } catch (Exception $e) {
                $helper->log('Error: ' . $e->getMessage());
                $helper->log('Trace: ' . $e->getTraceAsString());
            }
        }
        $this->unlock($lockId);
        return $this;
    }

    /**
     * Check if job is locked
     *
     * @param string $id
     * @return bool
     */
    public function isLocked($id)
    {
        if (isset($this->_isLocked[$id]) && $this->_isLocked[$id]) {
            return $this->_isLocked[$id];
        } else {
            $fp = $this->_getLockFile($id);
            if (flock($fp, LOCK_EX | LOCK_NB)) {
                flock($fp, LOCK_UN);
                return false;
            }
            return true;
        }
    }

    /**
     * Get lock file resource
     *
     * @param string $id
     * @return resource
     */
    protected function _getLockFile($id)
    {
        if (!isset($this->_lockFile[$id])) {
            $varDir = Mage::getConfig()->getVarDir('locks');
            $file = $varDir . DS . 'koin_job_' . $id . '.lock';
            if (is_file($file)) {
                $this->_lockFile[$id] = fopen($file, 'w');
            } else {
                $this->_lockFile[$id] = fopen($file, 'x');
            }
            fwrite($this->_lockFile[$id], date('r'));
        }
        return $this->_lockFile[$id];
    }

    /**
     * Lock job
     *
     * @param string $id
     * @return $this
     */
    public function lock($id)
    {
        $this->_isLocked[$id] = true;
        flock($this->_getLockFile($id), LOCK_EX | LOCK_NB);
        return $this;
    }

    /**
     * Unlock job
     *
     * @param string $id
     * @return $this
     */
    public function unlock($id)
    {
        $this->_isLocked[$id] = false;
        flock($this->_getLockFile($id), LOCK_UN);
        return $this;
    }

    /**
     * Suggest new Order
     *
     * @return $this
     */
    public function suggestNewOrder()
    {
        /** @var $helper Koin_Payment_Helper_Data */
        $helper = Mage::helper('koin');
        if (!$helper->getConfig('active') || !$helper->getConfig('suggest_new_order')) return $this;
        $lockId = 'suggest_new_order';
        if ($this->isLocked($lockId)) {
            return $this;
        }
        $this->lock($lockId);
        /** @var Mage_Sales_Model_Resource_Order_Collection $orders */
        $orders = Mage::getModel('sales/order')->getCollection();
        $orders->join(
            array('payment' => 'sales/order_payment'),
            'main_table.entity_id=payment.parent_id',
            array('payment_method' => 'payment.method')
        );
        $orders->addFieldToFilter('payment.method', array('in' => explode(',', $helper->getConfig('suggest_new_order_payment_methods'))));
        $orders->addFieldToFilter('status', array('in' => explode(',', $helper->getConfig('suggest_new_order_status'))));
        $orders->addFieldToFilter('created_at', array('gt' => new Zend_Db_Expr("DATE_ADD('" . now() . "', INTERVAL - 1 HOUR)")));
        /** @var Koin_Payment_Model_Email $email */
        $email = Mage::getModel('koin/email');
        if (!$orders) return $this;
        foreach ($orders as $order) {
            try {
                /** @var Mage_Sales_Model_Order $order */
                if ($order && $order->getId()) {
                    if (!$order->getPayment()->getAdditionalInformation('koin_suggest_new_order_sent')) {
                        $email->sendEmailSuggestNewOrder($order);
                        $order->getPayment()->setAdditionalInformation('koin_suggest_new_order_sent', 1);
                        $order->save();
                    }
                }
            } catch (Exception $e) {
                $helper->log('Error: ' . $e->getMessage());
                $helper->log('Trace: ' . $e->getTraceAsString());
            }
        }
        $this->unlock($lockId);
        return $this;
    }

    /**
     * Close file resource if it was opened
     */
    public function __destruct()
    {
        if ($this->_lockFile && is_array($this->_lockFile)) {
            foreach ($this->_lockFile as $file) {
                fclose($file);
            }
        }
    }

}

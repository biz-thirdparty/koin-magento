# KOIN payment method module for Magento.
Payment method module KOIN (v1.3.6) - Magento 1.7.x - 1.9.x


**[KOIN](https://www.koin.com.br/)** is the first  post-paid payment method of Latin America. With KOIN the customer only pays after receiving the product

With this module you'll be able to:
 - Make payments using **KOIN**
 - Update yours orders by cron when there's any change at KOIN.
 - Automatically chargeback when an order is cancelled
 - Automatically create the invoice if an order is approved
 - Suggest the customer to reorder using KOIN when an order is cancelled by using another payment method.
 - Send the Tracking Code to **KOIN**
 - Define minimum and maximum amount to show **KOIN**
 - Define by settings the address and customer attributes used in API
 - Show the method only to registered users in KOIN
 
koin-magento/skin/adminhtml/default/default/koin/
## Installation

### Modgit: [https://github.com/jreinke/modgit](https://github.com/jreinke/modgit)

    $ cd /path/to/magento
    $ modgit init
    $ modgit add koin-magento https://github.com/contardi/koin-magento.git

### Manual:

- Download the [latest version](https://github.com/contardi/koin-magento/archive/master.zip) of the module.
- Unzip the file, copy and paste into yout Magento folder.
- Clear the Cache.
- Go to `System > Configuration > Payment Methods Section`. There'll be a new section called "KOIN".
- Fill your KOIN credentials and enable the module


**[Documentação](http://developers.koin.com.br/ptbr/)**

**[Dúvidas](https://www.koin.com.br/fale-conosco/)**

